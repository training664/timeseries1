package com.zoho.crm.TestAPI.Service

import com.zoho.crm.TestAPI.Repository.{DataConstruction, PublishData, StoreData}
import com.zoho.crm.TestAPI.Utils.TestRequest

import java.util.logging.Logger

class TimeSeriesService {

    private val TimeSeriesServiceLogger: Logger = Logger.getLogger(this.getClass.getName)
    val dataConstruction = new DataConstruction
    val storeData = new StoreData
    val publishData = new PublishData
    
    def process(requestObj: TestRequest) : String =  {

      // Data Length > 10
      if(requestObj.time_series.getTime().length >= 10){
         val data = dataConstruction.constructTimeSeries(requestObj)
         var storageResult = storeData.writeinZOS(data)
         val status = publishData.publishZOSDatainRMQ("15942818")
         TimeSeriesServiceLogger.info("\n\n::ZOS \n\n")
         status
      }
      
      // if do_aggregate is true
      else if(requestObj.do_aggregate == true){
         val data = dataConstruction.aggregateData(requestObj)
         var storageResult = storeData.writeinZOS(data)
         val status = publishData.publishZOSDatainRMQ("15942818")
         TimeSeriesServiceLogger.info("\n\n::ZOS \n\n")
         status
      }
      
      // if DB data can be used
      else if(requestObj.use_db_data == true){
         val data = dataConstruction.getDataFromDB(requestObj)
         TimeSeriesServiceLogger.info(s"\n\ntime is :: ${data.getTime()} \n\n")
         TimeSeriesServiceLogger.info(s"\n\ntarget is :: ${data.getTarget()} \n\n")
         val zgId = "15942818"
         val dSInfo = storeData.writeinHDFS(requestObj, zgId)
         val status = publishData.publishinRMQ(dSInfo)
         TimeSeriesServiceLogger.info("\n\n::HDFS \n\n")
         status
      }
      
      else "FAILED"
    }
     
}