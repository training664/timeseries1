package com.zoho.crm.TestAPI.Repository
import com.zoho.crm.data.{DataSetProvider, PrestoClientObject, QueryType}
import com.zoho.crm.TestAPI.Utils.{TestRequest, TimeSeries}
import java.util.logging.{Level, Logger}
import com.zoho.crm.data.TempDSInfo

class StoreData {
  
     private val StoreDataLogger: Logger = Logger.getLogger(this.getClass.getName)    
  
  
     def writeinZOS(data : TimeSeries): String = {
      
      try{
         //ZosActions.putObject("common-bucket", "15942818", data) 
         StoreDataLogger.info("STORE SUCCESS")
      }
      catch{
          case e: Exception => StoreDataLogger.info(s"\n\n exception $e \n\n")
      }
      "SUCCESS"
   }
   
   def writeinHDFS(requestObject : TestRequest, zgId : String) : TempDSInfo = {
      
      val dspObj = new DataSetProvider
      val hdfsPath = "TimeSeriesAnalysis/" + zgId + "/" + requestObject.configId + "_pbdata.parquet"
      StoreDataLogger.info("In writeinHDFS")
      val selectQuery = "Select CALLSTARTDATETIME as Date,Count(*) as CallCount from CrmCall WHERE (((CrmCall.CALLID >= 111112000000000000) AND (CrmCall.CALLID <= 111112999999999999)) OR ((CrmCall.CALLID >= 0) AND (CrmCall.CALLID <= 999999999999))) GROUP BY CALLSTARTDATETIME"
      val dSInfo = dspObj.writeDataSetToTempHDFS( zgId, PrestoClientObject(query = selectQuery, writePath =hdfsPath, queryType =QueryType.PB_MODEL_TRAINING_QUERY))
      dSInfo
   }
   
}