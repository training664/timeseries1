package com.zoho.crm.TestAPI.Repository

import com.zoho.crm.rabbitmq.wrapper.PublisherWrapper
import com.zoho.crm.data.TempDSInfo
import java.util.logging.Logger


class PublishData {
  
   private val PublishDataLogger: Logger = Logger.getLogger(this.getClass.getName)

   def publishinRMQ(dSInfo : TempDSInfo): String = {
      val status=PublisherWrapper.publish("time_series_queue",dSInfo.path)
      PublishDataLogger.info("PUBLISHEDINRMQ::" + status)
      if(status==true)"SUCCESS" 
      else "FAILED"
   }

   def publishZOSDatainRMQ(zosPath: String): String = {
      val status = PublisherWrapper.publish("time_series_queue", zosPath)
      PublishDataLogger.info("PUBLISHEDINRMQ::" + status)
      if (status == true) "SUCCESS"
      else "FAILED"
   }
   
}