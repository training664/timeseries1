package com.zoho.crm.TestAPI.Repository

import com.adventnet.adventnetcrmsfa.{CRMCALL, CRMEVENT, CRMZIAMODELMETA}
import com.adventnet.db.api.RelationalAPI
import com.adventnet.ds.query._
import com.adventnet.persistence.{DataObject, Row}
import com.zoho.crm.core.components.PersistenceCacheUtil

import java.sql.Connection
import java.util.Arrays
import java.util.logging.Logger

class MickeyOperations {
  
  private val MickeyOperationsLogger: Logger = Logger.getLogger(this.getClass.getName)
  
    def getData() : DataSet = {

      val selectQuery = new SelectQueryImpl(Table.getTable(CRMCALL.TABLE))
      val timeColumn = Column.getColumn(CRMCALL.TABLE, CRMCALL.CALLSTARTDATETIME, CRMCALL.CALLID)
      val countColumn = Column.getColumn( CRMCALL.TABLE, CRMCALL.CALLID, "COUNT").count()
      selectQuery.addSelectColumn(timeColumn)
      selectQuery.addSelectColumn(countColumn)
      selectQuery.setGroupByClause(new GroupByClause(Arrays.asList(timeColumn)))

      val eventSelectQuery = new SelectQueryImpl(Table.getTable(CRMEVENT.TABLE))
      val eventTimeColumn = Column.getColumn(CRMEVENT.TABLE, CRMEVENT.EVENTSTARTDATETIME, CRMCALL.CALLID)
      val eventCountColumn = Column.getColumn(CRMEVENT.TABLE, CRMEVENT.EVENTID, "COUNT").count()
      eventSelectQuery.addSelectColumn(eventTimeColumn)
      eventSelectQuery.addSelectColumn(eventCountColumn)
      eventSelectQuery.setGroupByClause(new GroupByClause(Arrays.asList(eventTimeColumn)))

//      import com.adventnet.ds.query.Join
//      val join = new Join(CRMCALL.TABLE,CRMEVENT.TABLE , Array[String]("CALLID", "COUNT"), Array[String]("CALLID", "COUNT"), "CALL", "EVENT", Join.LEFT_JOIN)

      val unionQuery = new UnionQueryImpl(selectQuery, eventSelectQuery, false)

      var conn: Connection = null
      try {
        conn = RelationalAPI.getInstance().getConnection()
        var dataSet: DataSet = RelationalAPI.getInstance().executeQuery(selectQuery, conn)
        while (dataSet.next()) {
          System.out.println(s"\n\nCALL ID :: ${dataSet.getValue(1)} COUNT :: ${dataSet.getValue(2)}\n\n")
        }

        var dataSet2: DataSet = RelationalAPI.getInstance().executeQuery(unionQuery, conn)
        while (dataSet2.next()) {
          System.out.println(s"\n\nUNION CALL ID :: ${dataSet2.getValue(1)} COUNT :: ${dataSet2.getValue(2)}\n\n")
        }


        dataSet = RelationalAPI.getInstance().executeQuery(selectQuery, conn)
        MickeyOperationsLogger.info(s"\n\nData Fetched Successfully with datasize::  ${dataSet.getFetchSize}\n\n")
        dataSet
      }
    }

  
   def putData(configId : Long, metaInfo : String) {
     
      MickeyOperationsLogger.info("Reached putData")
      
      val persistence = PersistenceCacheUtil.getPersistenceLiteHandle
      val row = new Row(CRMZIAMODELMETA.TABLE)
      row.set(CRMZIAMODELMETA.CONFIGID, configId)
      row.set(CRMZIAMODELMETA.CONFIGTYPE, 1)
      row.set(CRMZIAMODELMETA.CONFIGRELATEDINFO, metaInfo)
      row.set(CRMZIAMODELMETA.MODELSTATUS, 1)
      row.set(CRMZIAMODELMETA.TOTALINFERENCE, 1)
      row.set(CRMZIAMODELMETA.SUCCESSFULINFERENCE, 1)
      row.set(CRMZIAMODELMETA.MODELACCURACY, 90.09)
      val dataObj = persistence.constructDataObject()
      dataObj.addRow(row)
      persistence.add(dataObj)
   }
}
