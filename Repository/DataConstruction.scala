package com.zoho.crm.TestAPI.Repository

import com.adventnet.ds.query.DataSet
import com.zoho.crm.TestAPI.Utils.{TestRequest, TimeSeries}
import java.sql.Timestamp

class DataConstruction {

   private val mickeyOperations = new MickeyOperations

   def convert(dataSet : DataSet): TimeSeries = {

  		var timeArray = new Array[String](50)
      var targetArray = new Array[BigDecimal](50)
  		var count = 0
      while (dataSet.next()) {
        var callCount = dataSet.getValue(2).asInstanceOf[Int].toLong
        var callStartDateTime = dataSet.getValue(1).asInstanceOf[Timestamp].toString()
        timeArray(count) = callStartDateTime
        targetArray(count) = callCount
        count += 1
      }
      val data = TimeSeries(timeArray.toList , targetArray.toList)
      data
   }


   def constructTimeSeries(requestObj: TestRequest) : TimeSeries =  {

        // Initialize
        var timeArray = new Array[String](50)
        var targetArray = new Array[BigDecimal](50)
        var time = requestObj.time_series.getTime()
        var target = requestObj.time_series.getTarget()
        var iter = 0
        while(iter<10){
          timeArray(iter) = time(iter)
          targetArray(iter) = target(iter)
          iter += 1
        }
        var data = TimeSeries(timeArray.toList.slice(0, 10),targetArray.toList.slice(0, 10))
        data
   }

   def aggregateData(requestObj: TestRequest) : TimeSeries =  {

        // Initialize
        val timeArray = new Array[String](50)
        val targetArray = new Array[BigDecimal](50)
        val time = requestObj.time_series.getTime()
        val target = requestObj.time_series.getTarget()
        var iter = 0
        while(iter<10){
          if(iter < time.size){
                timeArray(iter) = time(iter)
                targetArray(iter) = target(iter)
          }
          else{
                timeArray(iter) = time(0)
                targetArray(iter) = target(0)
          }
          iter+=1
        }
        var data = TimeSeries(timeArray.toList.slice(0, 10),targetArray.toList.slice(0, 10))
        data
   }

   def getDataFromDB(requestObj: TestRequest) : TimeSeries =  {

        // Initialize
        val timeArray = new Array[String](50)
        val targetArray = new Array[BigDecimal](50)

        val dataSet : DataSet = mickeyOperations.getData()
        val DBData  = convert(dataSet)
        val dbDataSize = dataSet.getFetchSize
        val DBTime = DBData.getTime()
        val DBTarget = DBData.getTarget()
        var iter = 0
        var count = 0
        while(iter<10){
          if(iter < requestObj.time_series.time.size){
                timeArray(iter) = requestObj.time_series.time(iter)
                targetArray(iter) = requestObj.time_series.target(iter)
          }
          else{
            timeArray(iter) = DBTime(count)
            targetArray(iter) = DBTarget(count)
            count += 1
          }
          iter += 1
          if(count>=dbDataSize) count = 0
        }
        val data = TimeSeries(timeArray.toList.slice(0, 10),targetArray.toList.slice(0, 10))
        data
   }

}