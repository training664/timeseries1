
package com.zoho.crm.TestAPI.Utils

import com.zoho.crm.intelligence.common.util.PythonNotificationListener
import java.util.logging.Logger
import com.google.gson.Gson
import play.api.libs.json.Json
import play.api.libs.json.JsValue
import com.zoho.crm.TestAPI.Repository.MickeyOperations


class TestCallBackListener extends PythonNotificationListener {
  
 private val LOG = Logger.getLogger(this.getClass.getName)

 override def execute(zgId : String, zosPath : String) : String = {

   LOG.info("\n\n\n\nTestListener :: notification recvd :: \n\n")
   LOG.info("ZGID is :: " + zgId + "ZOSPATH is :: " + zosPath + "\n\n\n")
   val mickeyOperations = new MickeyOperations
   
    var metaInfo = "{'success': True, "
    metaInfo += "'anomaly_scores': array([8.22734786e-02, 1.00000000e+00, 5.31792709e-02, 3.64001601e-01, "
    metaInfo += "4.46456347e-01, 3.59115718e-04, 1.32362335e-01, 8.27234489e-03,"
    metaInfo += "4.42832677e-02, 3.60352996e-01, 6.54219632e-02, 4.37352067e-02,"
    metaInfo += "1.64300803e-01, 0.00000000e+00, 3.23513130e-03, 8.96809391e-05]), "
    metaInfo += "'threshold': array([0.4285886]), "
    metaInfo += "'model': <src.time_series_anomaly_detection.anomaly_detectors.auto_regressor.AutoRegressor object at 0x1719272b0>, "
    metaInfo += "'min_max_scaler': MinMaxScaler(), "
    metaInfo += "'lags_used': 7,"
    metaInfo += "'next_run_time': Timestamp('2022-12-21 00:00:00', freq='D'), "
    metaInfo += "'next_run_time_in_millis': 1671580800000"
      
      
   mickeyOperations.putData(15942818, metaInfo)
   LOG.info("\n\nCOMPLETED PUTDATA\n\n")
   var respJson = Json.obj()
   respJson.toString()
 }
  
}