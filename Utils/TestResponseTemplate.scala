package com.zoho.crm.TestAPI.Utils

import com.zoho.crm.TestAPI.Utils.utils._
import com.fasterxml.jackson.databind.ObjectMapper


class TestResponseTemplate {
     
  def TestFailureResponse(message: String): String = {
    val failureResponse =  FailureResponse(status = "failure",  
      code = "INTERNAL_SERVER_ERROR",
      message = message)
    return failureResponse.toString
  }
  
  
  def TestErrorResponse(message: String): String = {
    val errorResponse =  ErrorResponse(status = "error",
      code = "INTERNAL_SERVER_ERROR",
      message = message)
    return errorResponse.toString
  }
   
   
  def TestSuccessResponse(response:Any):String = {
    val successResponse = SuccessResponse(
      status = "success",
      code = "SUCCESS",
      response = response
    )    
    return successResponse.toString
  }
    
}