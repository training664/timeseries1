package com.zoho.crm.TestAPI.Utils

import java.util.logging.Logger
import play.api.libs.json.{JsError, JsPath, JsSuccess, JsValue, Json, Reads}
import play.api.libs.functional.syntax._
import com.zoho.crm.common.utils.CommonUtils


class JsonConversionUtil {

  val testResponseTemplate = new TestResponseTemplate
  private val TestAPILogger: Logger = Logger.getLogger(this.getClass.getName)

  def getRequestObject(requestString: String): TestRequest = {

        implicit val timeSeries: Reads[TimeSeries] = (
        (JsPath   \ "time" ).read[List[String]] and
        (JsPath   \ "target").read[List[BigDecimal]]
        )(TimeSeries.apply _)

        implicit val requestObject: Reads[TestRequest] = (
          (JsPath \ "configId").read[String] and
          (JsPath \ "period").read[String] and
          (JsPath \ "period_id").read[Int] and
          (JsPath \ "do_aggregate").read[Boolean] and
          (JsPath \ "use_db_data").read[Boolean] and
          (JsPath \ "timeSeries" ).read[TimeSeries])(TestRequest.apply _)

        var json = Json.parse(requestString)
        json.validate[TestRequest] match {
          case JsSuccess(returnObject, _) => {
            returnObject
          }
          case e: JsError => {
            TestAPILogger.info("ERRORINJSON: " + e + "------------------")
            null
          }
        }
  }

  def getResponseObject(ZosActionResult: String): String = {
        try{
          val jsonResponse: JsValue = Json.parse(ZosActionResult)
          val strJsonResponse = Json.stringify(jsonResponse)
          testResponseTemplate.TestSuccessResponse(strJsonResponse)
        }
        catch {
          case th: Throwable => {
            TestAPILogger.severe("Exception_" + th.getStackTraceString)
            val throwable = CommonUtils.getStackTraceString(th)
            testResponseTemplate.TestErrorResponse(throwable)
          }
        }
      null
  }



}